package com.qd.pay.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 22:55
 */
@Data
public class QdPayType extends Param {

	private String name;

	private String code;
}
