package com.qd.pay.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 23:05
 */
@Data
public class QdPay extends Param {

	private String orderCode;

	private String callBackUrl;

	private String totalFee;

	private String subject;

	private String channel;
}
