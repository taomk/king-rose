package com.qd.pay.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 23:05
 */
@Data
public class QdPayRes extends Param {

    private String code;

    private String msg;

    private String url;
}
