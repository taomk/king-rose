package com.qd.pay.util;

import lombok.Getter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/16 11:59
 */
public enum QdPayType {

	QD_ALI_PAY("ali", 31),
	QD_WX_PAY("wx", 41);

	@Getter
	private String name;

	@Getter
	private int code;

	private QdPayType(String name, int code){
		this.name = name;
		this.code = code;
	}
}
