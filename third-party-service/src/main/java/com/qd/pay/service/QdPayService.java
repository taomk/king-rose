package com.qd.pay.service;

import java.util.List;

import com.qd.pay.model.QdPay;
import com.qd.pay.model.QdPayRes;
import com.qd.pay.model.QdPayType;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 22:50
 */
public interface QdPayService {

	/**
	 * 支付方式列表查询
	 * @return
	 */
	public List<QdPayType> findPayType();

	/**
	 * 千丁支付提交
	 * @param qdPay
	 * @param payTypeCode
	 * @return
	 */
	public QdPayRes pay(QdPay qdPay, String payTypeCode);
}
