package com.qd.pay.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.qd.json.GsonUtils;
import com.qd.string.StringExUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import com.qd.http.service.HttpAccessService;
import com.qd.pay.model.QdPay;
import com.qd.pay.model.QdPayRes;
import com.qd.pay.model.QdPayType;
import com.qd.pay.service.QdPayService;
import com.qd.util.AccessMethod;

import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 22:53
 */
@Slf4j
public class QdPayServiceImpl implements QdPayService {

	@Setter
	private HttpAccessService qdPayTypeAccessService;

	@Setter
	private HttpAccessService qdAliPayAccessService;

	@Setter
	private HttpAccessService qdWxPayAccessService;

	@Override
	public List<QdPayType> findPayType() {
		String res = qdPayTypeAccessService.access(AccessMethod.QD_PAY_TYPE, null);
        log.info("调用TC_SCENERY_LIST接口，访问結果为：" + res);
		// TODO res转QdPayType列表
        List<QdPayType> resultList = new ArrayList();
        if(StringExUtils.isNotNullOrEmpty(res)){
            List payTypeList = GsonUtils.fromJson(res, List.class);
            if(payTypeList != null && payTypeList.size()>0){
                for(Object obj : payTypeList){
                    QdPayType qdPayTyped = GsonUtils.fromJson(obj.toString(), QdPayType.class);
                    resultList.add(qdPayTyped);
                }
            }
        }
        log.info("QdPayServiceImpl.findPayType返回结果为：" + resultList);
		return resultList;
	}

	@Override
	public QdPayRes pay(QdPay qdPay, String payTypeCode) {

		String res = StringUtils.EMPTY;

		if (StringUtils.isBlank(payTypeCode)) {
			return null;
		}
        QdPayRes qdPayRes = new QdPayRes();
		if (payTypeCode.equals("31")) {//alipay
			res = qdAliPayAccessService.access(AccessMethod.QD_ALI_PAY, qdPay.toMap());//json
            Map<String, String> resultMap = GsonUtils.jsonToMap(res);
            qdPayRes.setCode(resultMap.get("code"));
            qdPayRes.setMsg(resultMap.get("msg"));
            qdPayRes.setUrl(resultMap.get("url"));
		} else if (payTypeCode.equals("41")) {//wxpay
			res = qdWxPayAccessService.access(AccessMethod.QD_WX_PAY, qdPay.toMap());//html页面
            qdPayRes.setCode("0");
            qdPayRes.setMsg("success");
            qdPayRes.setUrl(res);
		}
		// TODO res转QdPayRes
		return qdPayRes;
	}
}
