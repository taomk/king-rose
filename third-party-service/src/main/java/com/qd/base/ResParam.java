package com.qd.base;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 16:46
 */
@Data
public abstract class ResParam extends Param {

	private ResHeader header;
}
