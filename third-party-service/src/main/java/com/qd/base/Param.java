package com.qd.base;

import java.util.Map;

import com.qd.collection.MapExUtils;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 16:46
 */
public abstract class Param {

	public Map<String, String> toMap(){
		return MapExUtils.toStrMap(this);
	}
}
