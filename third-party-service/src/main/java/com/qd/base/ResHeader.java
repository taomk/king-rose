package com.qd.base;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 9:58
 */
@Data
public class ResHeader extends Param {

	private String actionCode;

	private String rspType;

	private String rspCode;

	private String rspDesc;

	private String digitalSign;

	private String rspTime;
}
