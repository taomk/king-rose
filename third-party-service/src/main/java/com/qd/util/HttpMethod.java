package com.qd.util;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/10 10:53
 */
public enum HttpMethod {

	GET, POST
}
