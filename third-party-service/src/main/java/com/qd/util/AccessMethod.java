package com.qd.util;

import lombok.Getter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/10 10:53
 */
public enum AccessMethod {

	TC_SCENERY_LIST("GetSceneryList"),
	TC_SCENERY_DETAIL("GetSceneryDetail"),
	TC_SCENERY_TRAFFIC_INFO("GetSceneryTrafficInfo"),
	TC_SCENERY_IMAGE_LIST("GetSceneryImageList"),
	TC_SCENERY_NEARBY("GetNearbyScenery"),
	TC_SCENERY_PRICE("GetSceneryPrice"),
	TC_SCENERY_PRICE_CALENDAR("GetPriceCalendar"),

	TC_SCENERY_ORDER_SUBMIT("SubmitSceneryOrder"),
	TC_SCENERY_ORDER_CANCEL("CancelSceneryOrder"),
	TC_SCENERY_ORDER_LIST("GetSceneryOrderList"),
	TC_SCENERY_ORDER_DETAIL("GetSceneryOrderDetail"),

	TC_SCENERY_PAY_SUBMIT("MobileWapPay"),

	TC_AREA_PROVINCE_LIST("GetProvinceList"),
	TC_AREA_CITY_LIST_BY_PROVINCE_ID("GetCityListByProvinceId"),
	TC_AREA_COUNTY_LIST_BY_CITY_ID("GetCountyListByCityId"),
	TC_AREA_DIVISION_BY_NAME("GetDivisionInfoByName"),
	TC_AREA_DIVISION_BY_ID("GetDivisionInfoById"),

	QD_PAY_TYPE("payType"),
	QD_ALI_PAY("aliPay"),
	QD_WX_PAY("wxPay");

	@Getter
	private String name;

	private AccessMethod(String name) {
		this.name = name;
	}
}
