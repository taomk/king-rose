package com.qd.http.service;

import java.util.Map;

import com.qd.util.AccessMethod;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/9 18:58
 */
public interface HttpAccessService {

	public String access(AccessMethod method, Map<String, String> input);
}
