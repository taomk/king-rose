package com.qd.http.service.impl;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.qd.http.service.HttpAccessService;
import com.qd.http.service.HttpClientService;
import com.qd.http.service.HttpSignService;
import com.qd.http.service.ReqParamService;
import com.qd.util.AccessMethod;

import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/9 19:13
 */
public class HttpAccessServiceImpl implements HttpAccessService {

	@Setter
	private HttpSignService httpSignService;

	@Setter
	private ReqParamService reqParamService;

	@Setter
	private HttpClientService httpClientService;

	@Override
	public String access(AccessMethod method, Map<String, String> input) {
		// 签名
		if (httpSignService != null) {
			input.putAll(httpSignService.sign(method.getName(), httpClientService.getUrl()));
		}

		// 请求参数
		String reqParam = StringUtils.EMPTY;
		if (reqParamService != null) {
			reqParam = reqParamService.createReqParam(input, method.getName());
		}

		// 访问
		String res = StringUtils.EMPTY;
		if (httpClientService != null) {
			res = httpClientService.doService(reqParam);
		}

		return res;
	}
}
