package com.qd.http.service;

import java.util.Map;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/9 19:55
 */
public interface HttpSignService {
	public Map<String, String> sign(String... param);
}