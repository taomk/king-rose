package com.qd.http.service.impl;

import org.apache.commons.lang3.StringUtils;

import com.qd.http.HttpClientUtils;
import com.qd.http.service.HttpClientService;
import com.qd.util.HttpMethod;

import lombok.Getter;
import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/10 10:44
 */
public class HttpClientServiceImpl implements HttpClientService {

	@Getter
	@Setter
	private String url;

	@Setter
	private String method = "GET";

	@Setter
	private String encode = "UTF-8";

	@Override
	public String doService(String reqParam) {

		HttpMethod curMethod = HttpMethod.valueOf(method);

		switch (curMethod) {
			case GET:
				return get(url, reqParam);
			case POST:
				return post(url, reqParam);
			default:
				return StringUtils.EMPTY;
		}
	}

	private String get(String url, String reqParam) {
		url += "?" + reqParam;
		return HttpClientUtils.sendGetRequest(url, encode);
	}

	private String post(String url, String reqParam) {
		return HttpClientUtils.sendPostRequest(url, reqParam, true);
	}
}
