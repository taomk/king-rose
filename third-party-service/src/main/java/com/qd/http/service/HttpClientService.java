package com.qd.http.service;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/10 9:44
 */
public interface HttpClientService {

	public String doService(String reqParam);

	public String getUrl();
}
