package com.qd.http.service;

import java.util.Map;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/9 19:51
 */
public interface ReqParamService {

	public String createReqParam(Map<String, String> input, String... other);
}
