package com.qd.http.qd.service.impl;

import java.util.Map;

import com.qd.http.service.ReqParamService;
import com.qd.string.StringExUtils;

import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 19:54
 */
public class QdPayReqParamer implements ReqParamService {

	@Setter
	private Map<String, String> reqStrMap;

	@Override
	public String createReqParam(Map<String, String> input, String... other) {
		return StringExUtils.replace(reqStrMap.get(other[0]), input);
	}
}
