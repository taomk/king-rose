package com.qd.http.tc.service.impl;

import java.util.HashMap;
import java.util.Map;

import com.qd.date.DateExUtils;
import com.qd.http.service.HttpSignService;
import com.qd.http.tc.util.TcSignUtils;

import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/9 19:28
 */
public class TcXmlSignServiceImpl implements HttpSignService {

	@Setter
	private String version;

	@Setter
	private String accountID;

	@Setter
	private String accountKey;

	@Override
	public Map<String, String> sign(String... param) {
		Map<String, String> sign = new HashMap<>();
		sign.put("version", version);
		sign.put("accountID", accountID);
		sign.put("accountKey", accountKey);
		sign.put("serviceName", param[0]);
		sign.put("reqTime", DateExUtils.currentDatetimeSSS());
		sign.put("digitalSign", TcSignUtils.createXmlSign(sign));
		return sign;
	}
}
