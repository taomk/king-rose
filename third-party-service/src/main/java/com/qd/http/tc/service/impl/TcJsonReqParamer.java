package com.qd.http.tc.service.impl;

import java.util.Map;

import com.qd.http.service.ReqParamService;
import com.qd.string.StringExUtils;

import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/9 20:04
 */
public class TcJsonReqParamer implements ReqParamService {

	@Setter
	private Map<String, String> reqJsonStrMap;

	@Override
	public String createReqParam(Map<String, String> input, String... other) {
		return StringExUtils.replace(reqJsonStrMap.get(other[0]), input);
	}
}
