package com.qd.http.tc.service.impl;

import java.util.HashMap;
import java.util.Map;

import com.qd.date.DateExUtils;
import com.qd.http.service.HttpSignService;
import com.qd.string.StringExUtils;
import com.qd.http.tc.util.TcSignUtils;

import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/9 19:28
 */
public class TcJsonSignServiceImpl implements HttpSignService {

	@Setter
	private String accountID;

	@Setter
	private String accountKey;

	@Setter
	private int allianceId;

	@Override
	public Map<String, String> sign(String... param) {

		String [] urlArrs = StringExUtils.splitStringToArray(param[1], "/");
		String module = urlArrs[urlArrs.length-3];
		String category = urlArrs[urlArrs.length-2];
		String reqTime = DateExUtils.currentDatetimeSSS();

		Map<String, String> sign = new HashMap<>();
		sign.put("allianceId", String.valueOf(allianceId));
		sign.put("reqTime", reqTime);
		sign.put("digitalSign", TcSignUtils.createJsonSign(module, category, param[0], accountID, accountKey, reqTime, allianceId));
		return sign;
	}
}
