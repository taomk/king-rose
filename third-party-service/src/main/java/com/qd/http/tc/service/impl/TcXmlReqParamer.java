package com.qd.http.tc.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.springframework.beans.factory.InitializingBean;

import com.qd.dom.XmlUtils;
import com.qd.http.service.ReqParamService;
import com.qd.string.StringExUtils;

import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/9 20:04
 */
public class TcXmlReqParamer implements ReqParamService, InitializingBean {

	@Setter
	private Map<String, String> reqXmlPathMap;

	private Map<String, String> reqParamStrMap;

	@Override
	public String createReqParam(Map<String, String> input, String... other) {
		return StringExUtils.replace(reqParamStrMap.get(other[0]), input);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		reqParamStrMap = new HashMap<>();
		for (String reqMethod : reqXmlPathMap.keySet()) {
			Document reqParamDoc = XmlUtils.parseFIOToXml(reqXmlPathMap.get(reqMethod));
			reqParamStrMap.put(reqMethod, XmlUtils.docToString(reqParamDoc));
		}
	}
}
