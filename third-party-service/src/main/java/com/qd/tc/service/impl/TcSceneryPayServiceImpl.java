package com.qd.tc.service.impl;

import com.qd.http.service.HttpAccessService;
import com.qd.json.GsonUtils;
import com.qd.tc.model.TcSceneryPay;
import com.qd.tc.model.TcSceneryPayRes;
import com.qd.tc.service.TcSceneryPayService;
import com.qd.util.AccessMethod;

import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/12 23:12
 */
public class TcSceneryPayServiceImpl implements TcSceneryPayService {

	@Setter
	private HttpAccessService httpAccessService;

	@Override
	public TcSceneryPayRes pay(TcSceneryPay sceneryPay) {
		String res = httpAccessService.access(AccessMethod.TC_SCENERY_PAY_SUBMIT, sceneryPay.toMap());
		return GsonUtils.fromJson(res, TcSceneryPayRes.class);
	}
}
