package com.qd.tc.service;

import java.util.List;

import com.qd.tc.model.*;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/12 23:09
 */
public interface TcSceneryService {

	/**
	 * 景区列表查询
	 * @param param
	 * @return
	 */
	public TcSceneryResEntity find(TcSceneryParam param);

	/**
	 * 根据景区ID查询景区详情
	 * @param sceneryId
	 * @return
	 */
	public TcSceneryDetailRes findById(String sceneryId);

	/**
	 * 景区主题列表查询
	 * @return
	 */
	public List<TcSceneryTheme> findThemes();

	/**
	 * 根据景区ID查询景区价格
	 * @param sceneryIds
	 * @return
	 */
	public List<TcSceneryPriceRes> findPriceById(String... sceneryIds);

    /**
     * 根据景区ID列表查询景区价格
     * @param sceneryIdList
     * @return
     */
    public List<TcSceneryPriceRes> findPriceByIdList(List<String> sceneryIdList);

	/**
	 * 根据景区政策ID查询价格日历
	 * @param policyId
	 * @return
	 */
	public TcSceneryPriceCalendarRes findPriceCalendarById(String policyId);

    /**
     * 根据就去ID查询景区图片
     * @param sceneryId
     * @param page
     * @param pageSize
     * @return
     */
    public TcSceneryImageRes findImageById(String sceneryId, int page, int pageSize);
}
