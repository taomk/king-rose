package com.qd.tc.service;

import com.qd.tc.model.TcSceneryPay;
import com.qd.tc.model.TcSceneryPayRes;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/12 23:09
 */
public interface TcSceneryPayService {

	/**
	 * 同程景区支付
	 * @param sceneryPay 支付信息
	 * @return 支付Res
	 */
	public TcSceneryPayRes pay(TcSceneryPay sceneryPay);
}
