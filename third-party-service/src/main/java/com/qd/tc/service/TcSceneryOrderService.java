package com.qd.tc.service;

import java.util.Map;

import com.qd.tc.model.TcSceneryOrder;
import com.qd.tc.model.TcSceneryOrderCancelRes;
import com.qd.tc.model.TcSceneryOrderSubmitRes;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/12 23:09
 */
public interface TcSceneryOrderService {

	/**
	 * 同程订单提交
	 * @param sceneryOrder 订单信息
	 * @return TcSceneryOrderRes 订单响应
	 */
	public TcSceneryOrderSubmitRes submit(TcSceneryOrder sceneryOrder);

	/**
	 * 同程订单取消
	 * @param serialId 订单流水号
	 * @param cancelReason 取消理由
	 * @return 0=失败, 1=成功
	 */
	public TcSceneryOrderCancelRes cancel(String serialId, String cancelReason);

	/**
	 * 同程订单状态
	 * @param serialIds 订单流水号
	 * @return 订单流水号=订单支付状态
	 */
	public Map<String, String> findOrderStatus(String... serialIds);
}
