package com.qd.tc.service.impl;

import com.qd.http.service.HttpAccessService;
import com.qd.tc.service.TcAreaService;

import lombok.Setter;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/12 23:08
 */
public class TcAreaServiceImpl implements TcAreaService {

	@Setter
	private HttpAccessService httpAccessService;
}
