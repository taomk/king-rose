package com.qd.tc.model;

import java.util.List;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 14:31
 */
@Data
public class TcSceneryOrder extends Param {

	public TcSceneryOrder (String sceneryId, String bMan, String bMobile, String tName, String tMobile, String policyId, String tickets, String travelDate, String orderIP) {
		this.sceneryId = sceneryId;
		this.bMan = bMan;
		this.bMobile = bMobile;
		this.tName = tName;
		this.tMobile = tMobile;
		this.policyId = policyId;
		this.tickets = tickets;
		this.travelDate = travelDate;
		this.orderIP = orderIP;
	}

	private String sceneryId;

	private String bMan;

	private String bMobile;

	private String bAddress;

	private String bPostCode;

	private String bEmail;

	private String tName;

	private String tMobile;

	private String policyId;

	private String tickets;

	private String travelDate;

	private String orderIP;

	private String idCard;

	private String otherGuest;

	private List<TcGuest> otherGuestList;
}
