package com.qd.tc.model;

import java.util.List;

import com.qd.base.ResParam;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 11:49
 */
@Data
public class TcSceneryPriceCalendarRes extends ResParam {

	private String sceneryId;

	private String sceneryName;

	private String pMode;

	private String useCard;

	private String realName;

	private String remark;

	private List<TcSceneryNotice> notices;

	private List<TcSceneryPriceCalendar> calendars;

	private TcSceneryAhead ahead;
}
