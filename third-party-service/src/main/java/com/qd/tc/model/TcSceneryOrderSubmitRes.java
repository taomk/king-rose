package com.qd.tc.model;

import com.qd.base.ResParam;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 14:31
 */
@Data
public class TcSceneryOrderSubmitRes extends ResParam {

	private String serialId;

	private String mseconds;
}
