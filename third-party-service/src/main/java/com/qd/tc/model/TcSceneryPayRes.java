package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 14:31
 */
@Data
public class TcSceneryPayRes extends Param {

	private String payUrl;

	private String respCode;

	private String respMsg;

	private String respTime;
}
