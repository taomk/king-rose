package com.qd.tc.model;

import com.qd.base.ResParam;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 14:31
 */
@Data
public class TcSceneryOrderCancelRes extends ResParam {

	private String isSuc;

	private String errMsg;
}
