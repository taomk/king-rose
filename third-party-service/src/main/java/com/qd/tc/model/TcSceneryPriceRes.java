package com.qd.tc.model;

import java.util.List;

import com.qd.base.ResParam;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 11:23
 */
@Data
public class TcSceneryPriceRes extends ResParam {

	private String sceneryId;

	private List<TcSceneryPolicy> policys;

	private List<TcSceneryNotice> notices;

	private TcSceneryAhead ahead;
}
