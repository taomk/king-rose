package com.qd.tc.model;

import java.util.List;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 12:30
 */
@Data
public class TcSceneryNotice extends Param {

	private String nType;

	private String nTypeName;

	private List<TcSceneryNoticeInfo> infos;
}
