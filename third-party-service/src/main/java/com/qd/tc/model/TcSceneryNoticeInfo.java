package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 13:03
 */
@Data
public class TcSceneryNoticeInfo extends Param {

	private String nId;

	private String nName;

	private String nContent;
}
