package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 16:44
 */
@Data
public class TcSceneryParam extends Param {

	public TcSceneryParam(String clientIp) {
		this.clientIp = clientIp;
	}

	private String clientIp;

	private String provinceId;

	private String cityId;

	private String countryId;

	private String page;

	private String pageSize;

	private String sortType;

	private String keyword;

	private String searchFields;

	private String gradeId;

	private String themeId;

	private String priceRange;

	private String cs;

	private String longitude;

	private String latitude;

	private String radius;

	private String payType;
}
