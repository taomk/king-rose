package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 14:18
 */
@Data
public class TcScenerySuitherd extends Param {

	private String suitherdId;

	private String suitherdName;
}
