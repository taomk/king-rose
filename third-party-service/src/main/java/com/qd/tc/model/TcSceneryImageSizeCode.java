package com.qd.tc.model;

import lombok.Data;

/**
 * 景点图片
 */
@Data
public class TcSceneryImageSizeCode {

	private String size;
    private String isDefault;
    private String sizeCode;

}
