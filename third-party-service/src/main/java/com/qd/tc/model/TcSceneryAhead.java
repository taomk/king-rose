package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 12:31
 */
@Data
public class TcSceneryAhead extends Param {

	private String day;

	private String time;
}
