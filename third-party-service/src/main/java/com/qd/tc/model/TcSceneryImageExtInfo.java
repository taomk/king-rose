package com.qd.tc.model;

import lombok.Data;

import java.util.List;

/**
 * 景点图片额外信息
 */
@Data
public class TcSceneryImageExtInfo {

	private String imageBaseUrl;

	private List<TcSceneryImageSizeCode> sizeCodeList;
}
