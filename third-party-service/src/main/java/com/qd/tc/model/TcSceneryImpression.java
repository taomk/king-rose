package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 14:21
 */
@Data
public class TcSceneryImpression extends Param {

	private String impressionId;

	private String impressionName;
}
