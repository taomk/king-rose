package com.qd.tc.model;


import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 14:43
 */
@Data
public class TcGuest extends Param {

	private String gName;

	private String gMobile;
}
