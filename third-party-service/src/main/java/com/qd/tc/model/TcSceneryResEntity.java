package com.qd.tc.model;

import com.qd.base.ResParam;
import lombok.Data;

import java.util.List;

/**
 * 同城景点列表信息
 * Created by txl on 2014/10/23.
 */
@Data
public class TcSceneryResEntity extends ResParam {

    private String page;

    private String pageSize;

    private String totalPage;

    private String totalCount;

    private String imgbaseURL;

    private List<TcSceneryRes> tcSceneryResList;
}
