package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 12:28
 */
@Data
public class TcSceneryPolicy extends Param {

	private String policyId;

	private String policyName;

	private String remark;

	private String price;

	private String tcPrice;

	private String pMode;

	private String gMode;

	private String minT;

	private String maxT;

	private String dpPrize;

	private String orderUrl;

	private String realName;

	private String useCard;

	private String ticketId;

	private String ticketName;

	private String bDate;

	private String eDate;

	private String openDateType;

	private String openDateValue;

	private String closeDate;

	private String advanceDay;

	private String timeLimit;

	private String containItems;
}
