package com.qd.tc.model;

import lombok.Data;

/**
 * 景点图片
 */
@Data
public class TcSceneryImage {

	private String imagePath;

}
