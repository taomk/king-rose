package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 14:31
 */
@Data
public class TcSceneryPay extends Param {

	public TcSceneryPay(String body, String quantity, String subject, String callBakUrl, String serialId) {
		this.body = body;
		this.quantity = quantity;
		this.subject = subject;
		this.callBakUrl = callBakUrl;
        this.serialId = serialId;
	}

	private String body;

	private String quantity;

	private String subject;

	private String buyerEmail;

	private String buyId;

	private String serialId;

	private String callBakUrl;

	private String amount;
}
