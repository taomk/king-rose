package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 12:51
 */
@Data
public class TcSceneryPriceCalendar extends Param {

	private String date;

	private String policyId;

	private String policyName;

	private String pMode;

	private String tcPrice;

	private String stock;

	private String minT;

	private String maxT;

	private String cash;

	private String remark;
}
