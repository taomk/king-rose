package com.qd.tc.model;

import com.qd.base.Param;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/14 11:01
 */
@Data
public class TcSceneryTheme extends Param {

	private String themeId;

	private String themeName;
}
