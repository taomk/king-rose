package com.qd.tc.model;

import java.util.List;

import com.qd.base.ResParam;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 14:18
 */
@Data
public class TcSceneryRes extends ResParam {

	private String sceneryName;

	private String sceneryId;

	private String sceneryAddress;

	private String scenerySummary;

	private String imgPath;

	private String provinceId;

	private String provinceName;

	private String cityId;

	private String cityName;

	private String countryId;

	private String countryName;

	private String gradeId;

	private String commentCount;

	private String questionCount;

	private String blogCount;

	private String viewCount;

	private String lon;

	private String lat;

	private String bookFlag;

	private List<TcSceneryTheme> themes;

	private List<TcScenerySuitherd> suitherds;

	private List<TcSceneryImpression> impressions;

	private List<String> naList;
}
