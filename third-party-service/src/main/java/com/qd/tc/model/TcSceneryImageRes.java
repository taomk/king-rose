package com.qd.tc.model;

import com.qd.base.ResParam;
import lombok.Data;

import java.util.List;

/**
 * 景点图片响应
 */
@Data
public class TcSceneryImageRes extends ResParam {

    private String page;

    private String pageSize;

    private String totalPage;

    private String totalCount;

	private List<TcSceneryImage> tcSceneryImageList;

	private TcSceneryImageExtInfo tcSceneryImageExtInfo;
}
