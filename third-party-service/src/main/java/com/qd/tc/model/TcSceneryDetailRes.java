package com.qd.tc.model;

import com.qd.base.ResParam;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/13 14:18
 */
@Data
public class TcSceneryDetailRes extends ResParam {

	private String sceneryId;

	private String sceneryName;

	private String gradeId;

	private String address;

	private String cityId;

	private String city;

	private String provinceId;

	private String province;

	private String intro;

	private String buyNotice;

	private String payMode;

	private String lon;

	private String lat;

	private String sceneryAlias;

	private String amountAdvice;

	private String ifUseCard;

    private int collectFlag;//收藏标志
}
