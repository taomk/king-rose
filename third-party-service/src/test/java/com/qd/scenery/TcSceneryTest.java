package com.qd.scenery;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.qd.base.ResHeader;
import com.qd.date.DateExUtils;
import com.qd.dom.XmlUtils;
import com.qd.http.service.HttpAccessService;
import com.qd.string.StringExUtils;
import com.qd.tc.model.*;
import com.qd.util.AccessMethod;
import com.qd.util.ReflectUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/applicationContext*.xml"})
public class TcSceneryTest {

	@Autowired
	@Qualifier("tcSceneryAccessService")
	private HttpAccessService httpAccessService;

    private void reflectResultToObj(List<Element> elementList, Object obj){
        reflectResultToObj(elementList, obj, null);
    }

    private void reflectResultToObj(List<Element> elementList, Object obj, Set<String> excludeFields){
        for(Element element : elementList){
            String fieldName = element.getName();//实体类属性值
            if(ReflectUtils.hasField(fieldName, obj) && (excludeFields ==null || !excludeFields.contains(fieldName))){
                ReflectUtils.setFieldValue(obj, fieldName, element.getStringValue());
            }
        }
    }

    @Test
    public void sceneryList() {

		InetAddress ip = null;
		try {
			ip = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			log.error("error exception", e);
			assert false;
		}

		Map<String, String> param = new HashMap<>();

		param.put("clientIp", ip.getHostAddress());
		param.put("provinceId", StringUtils.EMPTY);
		param.put("cityId", StringUtils.EMPTY);
		param.put("countryId", StringUtils.EMPTY);
		param.put("page", StringUtils.EMPTY);
		param.put("pageSize", StringUtils.EMPTY);
		param.put("sortType", StringUtils.EMPTY);
		param.put("keyword", StringUtils.EMPTY);
		param.put("searchFields", StringUtils.EMPTY);
		param.put("gradeId", StringUtils.EMPTY);
		param.put("themeId", StringUtils.EMPTY);
		param.put("priceRange", StringUtils.EMPTY);
		param.put("cs", StringUtils.EMPTY);
		param.put("longitude", StringUtils.EMPTY);
		param.put("latitude", StringUtils.EMPTY);
		param.put("radius", StringUtils.EMPTY);
		param.put("payType", StringUtils.EMPTY);

		String res = httpAccessService.access(AccessMethod.TC_SCENERY_LIST, param);
        List<TcSceneryRes> resultList = new ArrayList();
        Document doc = XmlUtils.parseStringToXml(res);
        Element root =  doc.getRootElement();
        System.out.println("----------------------");
        //解析header
        ResHeader resHeader = new ResHeader();
        List<Element> headerChildEleList =  root.element("header").elements();
        reflectResultToObj(headerChildEleList, resHeader);
        //解析body
        if(StringExUtils.isNotNullOrEmpty(resHeader.getRspType()) && resHeader.getRspType().equals("0")){
            List<Element> sceneryListChildEleList =  root.element("body").element("sceneryList").elements();//获取scenery元素列表
            for(Element element : sceneryListChildEleList){
                List<Element> sceneryChildEleList = element.elements();
                TcSceneryRes tcSceneryRes = new TcSceneryRes();
                reflectResultToObj(sceneryChildEleList, tcSceneryRes);
                tcSceneryRes.setHeader(resHeader);
                resultList.add(tcSceneryRes);
            }
        }
        System.out.println(resultList);
        System.out.println("----------------------");
	}

    @Test
    public void findById(){
        Map<String, String> param = new HashMap<>();
        param.put("sceneryId", String.valueOf(183478));
        param.put("cs", "2");
        String res = httpAccessService.access(AccessMethod.TC_SCENERY_DETAIL, param);
        // TODO res转TcSceneryDetailRes
        TcSceneryDetailRes tcSceneryDetailRes = new TcSceneryDetailRes();
        Document doc = XmlUtils.parseStringToXml(res);
        Element root =  doc.getRootElement();
        System.out.println("----------------------");
        //解析header
        ResHeader resHeader = new ResHeader();
        List<Element> headerList =  root.element("header").elements();
        reflectResultToObj(headerList, resHeader);
        tcSceneryDetailRes.setHeader(resHeader);
        //解析body
        List<Element> bodyList =  root.element("body").element("scenery").elements();
        reflectResultToObj(bodyList, tcSceneryDetailRes);
        System.out.println(tcSceneryDetailRes.getHeader());
        System.out.println(tcSceneryDetailRes);
        System.out.println("----------------------");

    }

    @Test
    public void findSceneryPriceById(){
        Map<String, String> param = new HashMap<>();
        param.put("showDetail", "1");
        param.put("sceneryIds", "183478");
        param.put("payType", "0");
        param.put("useCache", "0");

        String res = httpAccessService.access(AccessMethod.TC_SCENERY_PRICE, param);
        // TODO res转TcSceneryPriceRes集合
        List<TcSceneryPriceRes> resultList = new ArrayList();
        Document doc = XmlUtils.parseStringToXml(res);
        Element root =  doc.getRootElement();
        System.out.println("----------------------");
        System.out.println(res);
        //解析header
        ResHeader resHeader = new ResHeader();
        List<Element> headerList =  root.element("header").elements();
        reflectResultToObj(headerList, resHeader);
        //解析body（需要解析sceneryId，policys，notices，ahead）
        if(StringExUtils.isNotNullOrEmpty(resHeader.getRspType()) && resHeader.getRspType().equals("0")){
            List<Element> sceneryListChildEleList =  root.element("body").element("sceneryList").elements();//获取scenery元素列表
            for(Element element : sceneryListChildEleList){
                List<Element> sceneryChildEleList = element.elements();
                TcSceneryPriceRes tcSceneryPriceRes = new TcSceneryPriceRes();
                //解析sceneryId
                tcSceneryPriceRes.setSceneryId(element.element("sceneryId").getStringValue());
                //解析价格;
                List<Element> policyChildEleList = element.element("policy").elements(); //标签<p>的列表
                List<TcSceneryPolicy> policys = new ArrayList();
                for(Element pElement : policyChildEleList){
                    List<Element> ele = pElement.elements();
                    TcSceneryPolicy tcSceneryPolicy = new TcSceneryPolicy();
                    reflectResultToObj(ele, tcSceneryPolicy);
                    policys.add(tcSceneryPolicy);
                }
                tcSceneryPriceRes.setPolicys(policys);
                //解析notice
                List<Element> noticeChildEleList = element.element("notice").elements(); //标签<p>的列表
                List<TcSceneryNotice> notices = new ArrayList();
                for(Element nElement : noticeChildEleList){
                    List<Element> nChildEleList = nElement.elements();
                    TcSceneryNotice tcSceneryNotice = new TcSceneryNotice();
                    reflectResultToObj(nChildEleList, tcSceneryNotice);
                    //解析TcSceneryNotice内的infos
                    List<Element> nInfoChildEleList =  nElement.element("nInfo").elements();
                    for(Element infoElement : nInfoChildEleList){
                        TcSceneryNoticeInfo tcSceneryNoticeInfo = new TcSceneryNoticeInfo();
                        if(tcSceneryNotice.getInfos() == null){
                            tcSceneryNotice.setInfos(new ArrayList());
                        }
                        reflectResultToObj(infoElement.elements(), tcSceneryNoticeInfo);
                        tcSceneryNotice.getInfos().add(tcSceneryNoticeInfo);
                    }
                    notices.add(tcSceneryNotice);
                }
                //解析TcSceneryAhead
                TcSceneryAhead tcSceneryAhead = new TcSceneryAhead();
                List<Element> aheadChildEleList = element.element("ahead").elements();
                reflectResultToObj(aheadChildEleList, tcSceneryAhead);
                //组装
                System.out.println("policys.size() = " + policys.size() + "\n" + policys);
                tcSceneryPriceRes.setPolicys(policys);
                System.out.println("notices.size() = " + notices.size() + "\n" + notices);
                tcSceneryPriceRes.setNotices(notices);
                System.out.println(tcSceneryAhead);
                tcSceneryPriceRes.setAhead(tcSceneryAhead);
                resultList.add(tcSceneryPriceRes);
            }
        }
        System.out.println(resultList);
        System.out.println("----------------------");
    }

    @Test
    public void findPriceCalendarById(){
        Map<String, String> param = new HashMap<>();
        param.put("policyId", "112875");
        param.put("startDate", DateExUtils.currentDate());
        param.put("endDate", StringUtils.EMPTY);
        param.put("showDetail", "0");

        String res = httpAccessService.access(AccessMethod.TC_SCENERY_PRICE_CALENDAR, param);
        // TODO res转TcSceneryPriceCalendarRes
        System.out.println(res);
        TcSceneryPriceCalendarRes tcSceneryPriceCalendarRes = new TcSceneryPriceCalendarRes();
        Document doc = XmlUtils.parseStringToXml(res);
        Element root =  doc.getRootElement();
        //解析header
        ResHeader resHeader = new ResHeader();
        List<Element> headerList =  root.element("header").elements();
        reflectResultToObj(headerList, resHeader);
        //解析body
        if(StringExUtils.isNotNullOrEmpty(resHeader.getRspType()) && resHeader.getRspType().equals("0")){
            Element calendarInfoElement = root.element("body").element("calendarInfo");
            List<Element> calendarInfoChildEleList = calendarInfoElement.elements();//<calendarInfo>子标签列表
            Set<String> excludeFields = new HashSet();
            excludeFields.add("ahead");
            excludeFields.add("calendar");
            reflectResultToObj(calendarInfoChildEleList, tcSceneryPriceCalendarRes, excludeFields);
            //解析notices
            List<Element> noticeChildEleList = calendarInfoElement.element("notice").elements(); //标签<n>的列表
            List<TcSceneryNotice> notices = new ArrayList();
            for(Element nElement : noticeChildEleList){
                List<Element> nChildEleList = nElement.elements();
                TcSceneryNotice tcSceneryNotice = new TcSceneryNotice();
                reflectResultToObj(nChildEleList, tcSceneryNotice);
                //解析TcSceneryNotice内的infos
                List<Element> nInfoChildEleList =  nElement.element("nInfo").elements();
                for(Element infoElement : nInfoChildEleList){
                    TcSceneryNoticeInfo tcSceneryNoticeInfo = new TcSceneryNoticeInfo();
                    if(tcSceneryNotice.getInfos() == null){
                        tcSceneryNotice.setInfos(new ArrayList());
                    }
                    reflectResultToObj(infoElement.elements(), tcSceneryNoticeInfo);
                    tcSceneryNotice.getInfos().add(tcSceneryNoticeInfo);
                }
                notices.add(tcSceneryNotice);
            }
            //解析calendars
            List<Element> calendarChildEleList = calendarInfoElement.element("calendar").elements(); //标签<day>的列表
            List<TcSceneryPriceCalendar> calendars = new ArrayList();
            for(Element dayElement : calendarChildEleList){
                List<Element> dayChildEleList = dayElement.elements();
                TcSceneryPriceCalendar tcSceneryPriceCalendar = new TcSceneryPriceCalendar();
                reflectResultToObj(dayChildEleList, tcSceneryPriceCalendar);
                calendars.add(tcSceneryPriceCalendar);
            }
            //解析ahead
            TcSceneryAhead tcSceneryAhead = new TcSceneryAhead();
            List<Element> aheadChildEleList = calendarInfoElement.element("ahead").elements();
            reflectResultToObj(aheadChildEleList, tcSceneryAhead);
            //组装
            System.out.println(resHeader);
            tcSceneryPriceCalendarRes.setHeader(resHeader);
            System.out.println(notices);
            tcSceneryPriceCalendarRes.setNotices(notices);
            System.out.println(calendars);
            tcSceneryPriceCalendarRes.setCalendars(calendars);
            System.out.println(tcSceneryAhead);
            tcSceneryPriceCalendarRes.setAhead(tcSceneryAhead);
            System.out.println("-----------------------");
            System.out.println(tcSceneryPriceCalendarRes);
            System.out.println("-----------------------");
        }

    }

    @Test
    public void parseThemeList(){
        Document reqParamDoc = XmlUtils.parseFIOToXml("tongcheng/scenery/themes.xml");
        // TODO reqParamDoc转themeList
        List<TcSceneryTheme> themesEleList = new ArrayList();
        if(reqParamDoc!=null){
            Element rootEle = reqParamDoc.getRootElement();
            List<Element> themsEleList = rootEle.elements();
            for(Element element : themsEleList){
                TcSceneryTheme tcSceneryTheme = new TcSceneryTheme();
                tcSceneryTheme.setThemeId(element.element("id").getStringValue());
                tcSceneryTheme.setThemeName(element.element("name").getStringValue());
                themesEleList.add(tcSceneryTheme);
            }
        }
        System.out.println(themesEleList);
    }

}
