package com.qd.scenery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.Element;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.qd.base.ResHeader;
import com.qd.dom.XmlUtils;
import com.qd.http.service.HttpAccessService;
import com.qd.string.StringExUtils;
import com.qd.tc.model.TcSceneryOrderCancelRes;
import com.qd.tc.model.TcSceneryOrderSubmitRes;
import com.qd.util.ReflectUtils;

/**
 * 同城景区订单测试用例
 * User: haowenchao
 * Date: 14-10-15
 * Time: 上午9:14
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/applicationContext*.xml"})
public class TcSceneryOrderTest {

    @Autowired
    @Qualifier("tcSceneryAccessService")
    private HttpAccessService httpAccessService;

    private void reflectResultToObj(List<Element> elementList, Object obj){
        reflectResultToObj(elementList, obj, null);
    }

    private void reflectResultToObj(List<Element> elementList, Object obj, Set<String> excludeFields){
        for(Element element : elementList){
            String fieldName = element.getName();//实体类属性值
            if(ReflectUtils.hasField(fieldName, obj) && (excludeFields ==null || !excludeFields.contains(fieldName))){
                ReflectUtils.setFieldValue(obj, fieldName, element.getStringValue());
            }
        }
    }

    private void reflectResultToMap(List<Element> elementList, Map<String, String> map){
        reflectResultToMap(elementList, map, null);
    }

    private void reflectResultToMap(List<Element> elementList, Map<String, String> map, Set<String> excludeFields){
        Set<String> keySet = map.keySet();
        for(Element element : elementList){
            String fieldName = element.getName();//实体类属性值
            if(keySet.contains(fieldName) && (excludeFields ==null || !excludeFields.contains(fieldName))){
                map.put(fieldName, element.getStringValue());
            }
        }
    }

    @Test
    public void submit(){
        // TODO res转TcSceneryOrderRes
        String res = "<response>\n" +
                "  <header>\n" +
                "    <actionCode>1</actionCode>\n" +
                "    <rspType>0</rspType>\n" +
                "    <rspCode>0000</rspCode>\n" +
                "    <rspDesc>1</rspDesc>\n" +
                "    <digitalSign/>\n" +
                "    <rspTime>2012-07-19 15:49:00.130</rspTime>\n" +
                "  </header>\n" +
                "  <body>\n" +
                "    <order>\n" +
                "      <serialId>123</serialId>\n" +
                "      <mseconds>11111</mseconds>\n" +
                "    </order>\n" +
                "  </body>\n" +
                "</response>\n";

        TcSceneryOrderSubmitRes tcSceneryOrderSubmitRes =  new TcSceneryOrderSubmitRes();
        Document doc = XmlUtils.parseStringToXml(res);
        Element root =  doc.getRootElement();
        System.out.println("----------------------");
        //解析header
        ResHeader resHeader = new ResHeader();
        List<Element> headerChildEleList =  root.element("header").elements();
        reflectResultToObj(headerChildEleList, resHeader);
        tcSceneryOrderSubmitRes.setHeader(resHeader);
        //解析body
        if(StringExUtils.isNotNullOrEmpty(resHeader.getRspType()) && resHeader.getRspType().equals("0")){
            Element orderEle =  root.element("body").element("order");//获取order元素列表
            reflectResultToObj(orderEle.elements(), tcSceneryOrderSubmitRes);
        }
        System.out.println(tcSceneryOrderSubmitRes);
        System.out.println("----------------------");
    }

    @Test
    public void cancel(){
        Map<String, String> param = new HashMap<>();
        param.put("serialId", "");
        param.put("cancelReason", "其他");

        // TODO res转TcSceneryOrderCancelRes
        String res = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<response>\n" +
                "  <header>\n" +
                "    <actionCode>1</actionCode>\n" +
                "    <rspType>0</rspType>\n" +
                "    <rspCode>0000</rspCode>\n" +
                "    <rspDesc><![CDATA[1]]></rspDesc>\n" +
                "    <digitalSign/>\n" +
                "    <rspTime>2014-10-14 19:29:18.447</rspTime>\n" +
                "  </header>\n" +
                "  <body>\n" +
                "    <isSuc>1</isSuc>\n" +
                "    <errMsg>取消成功！</errMsg>\n" +
                "  </body>\n" +
                "</response>";
        TcSceneryOrderCancelRes tcSceneryOrderCancelRes = new TcSceneryOrderCancelRes();
        Document doc = XmlUtils.parseStringToXml(res);
        Element root =  doc.getRootElement();
        System.out.println("----------------------");
        //解析header
        ResHeader resHeader = new ResHeader();
        List<Element> headerChildEleList =  root.element("header").elements();
        reflectResultToObj(headerChildEleList, resHeader);
        tcSceneryOrderCancelRes.setHeader(resHeader);
        //解析body
        if(StringExUtils.isNotNullOrEmpty(resHeader.getRspType()) && resHeader.getRspType().equals("0")){
            List<Element> bodyChildEleList =  root.element("body").elements();//获取scenery元素列表
            reflectResultToObj(bodyChildEleList, tcSceneryOrderCancelRes);
        }
        System.out.println(tcSceneryOrderCancelRes);
        System.out.println("----------------------");
    }

    @Test
    public void findOrderStatusBySerialIds(){
        Map<String, String> param = new HashMap<>();
        param.put("serialIds", "");
        param.put("writeDB", "0");
        //String res = httpAccessService.access(AccessMethod.TC_SCENERY_ORDER_DETAIL, param);
        String res = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<response>\n" +
                "  <header>\n" +
                "    <actionCode>1</actionCode>\n" +
                "    <rspType>0</rspType>\n" +
                "    <rspCode>0000</rspCode>\n" +
                "    <rspDesc><![CDATA[获取订单结果成功]]></rspDesc>\n" +
                "    <digitalSign />\n" +
                "    <rspTime>2012-10-19 16:14:28.978</rspTime>\n" +
                "  </header>\n" +
                "  <body>\n" +
                "    <orderList>\n" +
                "      <order>\n" +
                "        <serialId>123</serialId>\n" +
                "        <orderStatus>1</orderStatus>\n" +
                "        <createDate>{ createDate }</createDate>\n" +
                "        <travelDate>{ travelDate }</travelDate>\n" +
                "        <sceneryId>{ sceneryId }</sceneryId>\n" +
                "        <sceneryName>{ sceneryName }</sceneryName>\n" +
                "        <bookingMan>{ bookingMan }</bookingMan>\n" +
                "        <bookingMobile>{ bookingMobile }</bookingMobile>\n" +
                "        <guestName>{ guestName }</guestName>\n" +
                "        <guestMobile>{ guestMobile }</guestMobile>\n" +
                "        <ticketName>{ ticketName }</ticketName>\n" +
                "        <ticketTypeId>{ ticketTypeId }</ticketTypeId>\n" +
                "        <ticketQuantity>{ ticketQuantity }</ticketQuantity>\n" +
                "        <ticketPrice>{ ticketPrice }</ticketPrice>\n" +
                "        <ticketAmount>{ ticketAmount }</ticketAmount>\n" +
                "        <prizeAmount>{ prizeAmount }</prizeAmount>\n" +
                "        <payStatus>{ payStatus }</payStatus>\n" +
                "        <enableCancel>{ enableCancel }</enableCancel>\n" +
                "        <currentPayStatus>{ currentPayStatus }</currentPayStatus >\n" +
                "      </order>\n" +
                "      <order>\n" +
                "        <serialId>124</serialId>\n" +
                "        <orderStatus>1</orderStatus>\n" +
                "        <createDate>{ createDate }</createDate>\n" +
                "        <travelDate>{ travelDate }</travelDate>\n" +
                "        <sceneryId>{ sceneryId }</sceneryId>\n" +
                "        <sceneryName>{ sceneryName }</sceneryName>\n" +
                "        <bookingMan>{ bookingMan }</bookingMan>\n" +
                "        <bookingMobile>{ bookingMobile }</bookingMobile>\n" +
                "        <guestName>{ guestName }</guestName>\n" +
                "        <guestMobile>{ guestMobile }</guestMobile>\n" +
                "        <ticketName>{ ticketName }</ticketName>\n" +
                "        <ticketTypeId>{ ticketTypeId }</ticketTypeId>\n" +
                "        <ticketQuantity>{ ticketQuantity }</ticketQuantity>\n" +
                "        <ticketPrice>{ ticketPrice }</ticketPrice>\n" +
                "        <ticketAmount>{ ticketAmount }</ticketAmount>\n" +
                "        <prizeAmount>{ prizeAmount }</prizeAmount>\n" +
                "        <payStatus>{ payStatus }</payStatus>\n" +
                "        <enableCancel>{ enableCancel }</enableCancel>\n" +
                "        <currentPayStatus>{ currentPayStatus }</currentPayStatus >\n" +
                "      </order>\n" +
                "    </orderList>\n" +
                "  </body>\n" +
                "</response>\n";
        // TODO 从res中解析出每个订单的支付状态
        Map<String, String> resultMap = new HashMap();
        Document doc = XmlUtils.parseStringToXml(res);
        Element root =  doc.getRootElement();
        System.out.println("----------------------");
        //解析header
        ResHeader resHeader = new ResHeader();
        List<Element> headerChildEleList =  root.element("header").elements();
        reflectResultToObj(headerChildEleList, resHeader);
        //解析body
        if(StringExUtils.isNotNullOrEmpty(resHeader.getRspType()) && resHeader.getRspType().equals("0")){
            Element orderListEle =  root.element("body").element("orderList");//获取orderList元素
            if(orderListEle != null){
                List<Element> orderEleList = orderListEle.elements();
                for (Element orderChildEle : orderEleList){
                    Map<String, String> orderMap = new HashMap();
                    orderMap.put("serialId", "");
                    orderMap.put("orderStatus", "");
                    reflectResultToMap(orderChildEle.elements(), orderMap);
                    resultMap.put(orderMap.get("serialId"), orderMap.get("orderStatus"));
                }
            }
        }
        System.out.println(resultMap);
        System.out.println("----------------------");
    }

}
