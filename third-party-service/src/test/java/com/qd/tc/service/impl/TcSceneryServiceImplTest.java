package com.qd.tc.service.impl;

import java.net.InetAddress;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.qd.json.GsonUtils;
import com.qd.tc.model.*;

/**
 * TcSceneryServiceImpl测试用例
 * User: haowenchao
 * Date: 14-10-15
 * Time: 下午1:57
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/applicationContext*.xml"})
public class TcSceneryServiceImplTest {

    @Autowired
    TcSceneryServiceImpl tcSceneryServiceImpl;

    @Test
    public void testFind() throws Exception {
        TcSceneryParam param = new TcSceneryParam(InetAddress.getLocalHost().toString());
        param.setThemeId("10");
        TcSceneryResEntity tcSceneryResEntity = tcSceneryServiceImpl.find(param);
        System.out.println(GsonUtils.toJson(tcSceneryResEntity));
    }

    @Test
    public void testFindById() throws Exception {
        TcSceneryDetailRes tcSceneryDetailRes = tcSceneryServiceImpl.findById("183501");
        System.out.println(tcSceneryDetailRes);
    }

    @Test
    public void testFindPriceById() throws Exception {
        List<TcSceneryPriceRes> reulstList = tcSceneryServiceImpl.findPriceById("183493,183494");
    }

    @Test
    public void testFindPriceCalendarById() throws Exception {
        TcSceneryPriceCalendarRes tcSceneryPriceCalendarRes = tcSceneryServiceImpl.findPriceCalendarById("113015");
    }

    @Test
    public void testFindThemes() throws Exception {
        List<TcSceneryTheme>  resultList = tcSceneryServiceImpl.findThemes();
        System.out.println(resultList);
    }

    @Test
    public void testFindImageById() throws Exception {
        TcSceneryImageRes tcSceneryImageRes = tcSceneryServiceImpl.findImageById("183493", 1, 10);
        System.out.println(GsonUtils.toJson(tcSceneryImageRes));
    }
}
