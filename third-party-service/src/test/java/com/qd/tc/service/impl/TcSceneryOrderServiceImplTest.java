package com.qd.tc.service.impl;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.qd.tc.model.TcSceneryOrder;

/**
 * TcSceneryOrderServiceImpl测试用例
 * User: haowenchao
 * Date: 14-10-15
 * Time: 下午3:10
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:/applicationContext*.xml"})
public class TcSceneryOrderServiceImplTest {

    @Autowired
    TcSceneryOrderServiceImpl tcSceneryOrderServiceImpl;

    @Test
    public void testSubmit() throws Exception {
        //String sceneryId, String bMan, String bMobile, String tName, String tMobile, String policyId, String tickets, String travelDate, String orderIP
        TcSceneryOrder sceneryOrder = new TcSceneryOrder(
            "183478",
            "吴仁润",
            "15901116139",
            "吴仁润",
            "15901116139",
            "112875",
            "1",
            "2014-10-20",
            "192.168.1.1"
        );
        sceneryOrder.setBAddress("北京市朝阳区常营龙湖长楹天街西1区1楼2601");
        if(sceneryOrder.getOtherGuestList() == null){
            sceneryOrder.setOtherGuestList(new ArrayList());
        }
        tcSceneryOrderServiceImpl.submit(sceneryOrder);
    }

    @Test
    public void testCancel() throws Exception {
        tcSceneryOrderServiceImpl.cancel("st543u52u9210034d643", "1");
    }

    @Test
    public void testFindOrderStatus() throws Exception {
        tcSceneryOrderServiceImpl.findOrderStatus("123,1235");
    }
}
