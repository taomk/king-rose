package com.king.task;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseScheduleTask implements Runnable {

	@Override
	public void run() {
		try {
			if (shouldTaskRun()) {
				StopWatch watch = new StopWatch();
				log.info("Schedule Task [" + getClass() + "] start");
				watch.start();
				runTask();
				watch.stop();
				log.info("Schedule Task [" + getClass() + "] finished, time = " + (watch.getTime() / 1000) + "s");
			}
		} catch (Exception e) {
			log.error("Schedule Task [" + getClass() + "] is failed", e);
		}
	}

	protected boolean shouldTaskRun() {
		boolean scheduleTaskEnable = false;
		String sysProperty = StringUtils.trimToEmpty(System.getProperty("SCHEDULE_TASK_ENABLE"));
		if (StringUtils.equals(sysProperty, "true")) {
			scheduleTaskEnable = true;
		}
		return scheduleTaskEnable;
	}

	protected abstract void runTask() throws Exception;
}