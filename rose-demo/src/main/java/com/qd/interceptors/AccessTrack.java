package com.qd.interceptors;

import java.lang.annotation.*;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/17 10:47
 */
@Inherited
@Target( { ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AccessTrack {
}
