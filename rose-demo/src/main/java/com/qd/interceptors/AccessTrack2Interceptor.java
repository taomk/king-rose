package com.qd.interceptors;

import net.paoding.rose.web.ControllerInterceptorAdapter;
import net.paoding.rose.web.Dispatcher;
import net.paoding.rose.web.Invocation;
import net.paoding.rose.web.InvocationChain;
import net.paoding.rose.web.annotation.Interceptor;

import java.lang.reflect.Method;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/17 10:26
 */
@Interceptor(oncePerRequest = true)
public class AccessTrack2Interceptor extends ControllerInterceptorAdapter {

	@Override
	public Object before(Invocation inv) throws Exception {
		System.out.println("Execute AccessTrack2.before()");
		return super.before(inv);
	}

	@Override
	protected Object after(Invocation inv, Object instruction) throws Exception {
		System.out.println("Execute AccessTrack2.after()");
		return instruction;
	}

	@Override
	public void afterCompletion(final Invocation inv, Throwable ex) throws Exception {
		System.out.println("Execute AccessTrack2.afterCompletion()");
	}

	@Override
	protected boolean isForAction(Method actionMethod, Class<?> controllerClazz) {
		System.out.println("Execute AccessTrack2.isForAction()");
		return super.isForAction(actionMethod, controllerClazz);
	}

	@Override
	public boolean isForDispatcher(Dispatcher dispatcher) {
		System.out.println("Execute AccessTrack2.isForDispatcher()");
		return super.isForDispatcher(dispatcher);
	}

	@Override
	public void setPriority(int priority) {
		System.out.println("Execute AccessTrack2.setPriority()");
		super.setPriority(priority);
	}

	@Override
	protected Object round(Invocation inv, InvocationChain chain) throws Exception {
		System.out.println("Execute AccessTrack2.round()");
		return super.round(inv, chain);
	}
}
