package com.qd.handlers;

import net.paoding.rose.web.ControllerErrorHandler;
import net.paoding.rose.web.Invocation;
import net.paoding.rose.web.ParentErrorHandler;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/17 16:38
 */
public class MyErrorHandler implements ControllerErrorHandler, InitializingBean {

	@Autowired
	private ParentErrorHandler parentErrorHandler;

	public Object onError(Invocation inv, NullPointerException ex) {
		return "/views/500.jsp";
	}

	public Object onError(Invocation inv, RuntimeException ex) throws Throwable {
		System.out.println("---------RuntimeException----------");
		inv.getResponse().getWriter().write("<pre>RuntimeException<br>");
		ex.printStackTrace(inv.getResponse().getWriter());
		inv.getResponse().getWriter().write("</pre>");
		return "";
	}

	@Override
	public Object onError(Invocation inv, Throwable ex) throws Throwable {
		return parentErrorHandler.onError(inv, ex);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("MyErrorHandler afterPropertiesSet");
	}
}
