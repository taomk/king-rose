package com.qd.user.dao;

import net.paoding.rose.jade.annotation.*;

import java.util.List;

import com.qd.user.model.ShardUser;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/19 12:30
 */
@DAO
public interface UserDAO {

	@SQL("SELECT id, name FROM user")
	public List<ShardUser> getUsersByShardId(@ShardBy @SQLParam("id") int shardId);

	@ReturnGeneratedKeys
	@SQL("INSERT INTO user (name) values (:u.name)")
	public int insertUserByShardId(@ShardBy @SQLParam("id") int shardId, @SQLParam("u") ShardUser user);

	@SQL("SELECT id, name FROM user WHERE name=:name ")
	public ShardUser getUserByName(@ShardBy @SQLParam("id") int shardId, @SQLParam("name") String name);
}
