package com.qd.user.model;

import com.qd.model.Level;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/16 21:58
 */
@Data
public class ShardUser {

	private long id;

	private String name;

	private Level level;
}
