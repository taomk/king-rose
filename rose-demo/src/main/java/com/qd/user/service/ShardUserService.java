package com.qd.user.service;

import java.util.List;

import com.qd.user.model.ShardUser;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/19 16:07
 */
public interface ShardUserService {

	public List<ShardUser> findAll();

	public int add(ShardUser user);

	public ShardUser findByName(String name);
}
