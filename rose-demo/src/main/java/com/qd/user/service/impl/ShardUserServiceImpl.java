package com.qd.user.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qd.user.dao.UserDAO;
import com.qd.user.model.ShardUser;
import com.qd.user.service.ShardUserService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/19 16:07
 */
@Service
public class ShardUserServiceImpl implements ShardUserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public List<ShardUser> findAll() {

		List<ShardUser> userList = new ArrayList<ShardUser>();

		for (int i =0; i < 10; i++) {

			userList.addAll(userDAO.getUsersByShardId(i));
		}

		return userList;
	}

	@Override
	public int add(ShardUser user) {
		return userDAO.insertUserByShardId(user.getName().hashCode(), user);
	}

	@Override
	public ShardUser findByName(String name) {
		return userDAO.getUserByName(name.hashCode(), name);
	}
}
