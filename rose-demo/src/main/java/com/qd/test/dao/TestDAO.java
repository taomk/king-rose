package com.qd.test.dao;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.ReturnGeneratedKeys;
import net.paoding.rose.jade.annotation.SQL;
import net.paoding.rose.jade.annotation.SQLParam;

import com.qd.test.model.Test;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/19 13:31
 */
@DAO
public interface TestDAO {
	@SQL("SELECT id, msg FROM test LIMIT 1")
	public Test getTest();

	@ReturnGeneratedKeys
	@SQL("insert into test (msg) values (:t.msg)")
	public int insertTest(@SQLParam("t") Test test);
}
