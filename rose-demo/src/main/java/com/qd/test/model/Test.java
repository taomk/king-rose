package com.qd.test.model;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/19 13:32
 */
public class Test {

	private long id;

	private String msg;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "Test{" +
				"id=" + id +
				", msg='" + msg + '\'' +
				'}';
	}
}
