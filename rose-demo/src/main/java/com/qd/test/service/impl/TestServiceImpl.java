package com.qd.test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qd.test.dao.TestDAO;
import com.qd.test.model.Test;
import com.qd.test.service.TestService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/19 13:35
 */
@Service
public class TestServiceImpl implements TestService {

	@Autowired
	private TestDAO testDAO;

	@Override
	public Test getTest() {
		return testDAO.getTest();
	}

	@Override
	public int insertTest(Test test) {
		return testDAO.insertTest(test);
	}
}
