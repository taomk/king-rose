package com.qd.test.service;

import com.qd.test.model.Test;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/19 13:34
 */
public interface TestService {
	public Test getTest();

	public int insertTest(Test test);
}
