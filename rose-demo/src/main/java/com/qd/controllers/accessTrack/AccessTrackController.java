package com.qd.controllers.accessTrack;

import net.paoding.rose.web.annotation.Path;

import com.qd.interceptors.AccessTrack;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/17 11:11
 */
@AccessTrack
@Path("")
public class AccessTrackController {

	public String interceptor() {
		return "@interceptor";
	}
}
