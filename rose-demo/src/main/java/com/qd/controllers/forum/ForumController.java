package com.qd.controllers.forum;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;
import net.paoding.rose.web.annotation.rest.Post;
import net.paoding.rose.web.var.Model;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/16 10:32
 */
@Path("")
public class ForumController {

	@Get("topic")
	public String getTopics() {
		//显示主帖列表
		return "topiclist";
	}

	@Get("topic/{topicId:[0-9]+}")
	public String showTopic(Model model, @Param("topicId") int topicId) {
		//显示单个主帖和它的跟贴
		model.add("id", topicId);
		return "topic";
	}

	@Get("topic/{topicId:[0-9]+}/comment/{commentId:[0-9]+}")
	public String showComment(Model model, @Param("topicId") int topicId, @Param("commentId") int commentId) {
		//显示单个跟贴
		model.add("topicId", topicId);
		model.add("commentId", commentId);
		return "comment";
	}

	@Post("topic")
	public String createTopic(){
		//创建一个主帖
		return "topic";
	}
	@Post("topic/{topicId:[0-9]+}/comment")
	public String createComment(Model model, @Param("topicId") int topicId){
		//创建一个跟贴
		model.add("topicId", topicId);
		return "comment";
	}
}
