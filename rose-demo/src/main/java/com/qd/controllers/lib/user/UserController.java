package com.qd.controllers.lib.user;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Delete;
import net.paoding.rose.web.annotation.rest.Get;
import net.paoding.rose.web.annotation.rest.Post;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.qd.controllers.lib.LoginRequired;
import com.qd.lib.model.User;
import com.qd.lib.service.UserService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 12:31
 */
@LoginRequired
@Path("")
public class UserController {

	@Autowired
	private UserService userService;

	// 11)
	@Get
	public String list() {
		List<User> userList = userService.find();
		return "@user-list:" + userList.toString();
	}

	// 12)
	@Post
	public String add(User user) {

		user.setCreateTime(new Date());

		long id = userService.save(user);
		if (id > 0) {
			return "r:/lib/user";
		} else {
			return "@user-add";
		}
	}

	// 13)
	@Get("add")
	public String showAdd() {
		return "@user-add";
	}

	// 14)
	@Get("{userId}")
	public String show(@Param("userId") String userId, @Param("isEdit") boolean isEdit) {
		if (isEdit) {
			return "@user-edit";
		}
		return "@user-page";
	}

	// 15)
	@Delete("{userId}")
	public String delete(@Param("userId") String userId) {
		return "r:/lib/user";
	}
}
