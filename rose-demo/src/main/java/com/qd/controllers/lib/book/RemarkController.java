package com.qd.controllers.lib.book;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Delete;

import org.springframework.beans.factory.annotation.Autowired;

import com.qd.controllers.lib.LoginRequired;
import com.qd.lib.service.RemarkService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 12:30
 */
@LoginRequired
@Path("{bookId:[0-9]+}/remark")
public class RemarkController {

	@Autowired
	private RemarkService remarkService;

	// 7)
	@Delete
	public String clear(@Param("bookId") long bookId) {
		remarkService.removeByBook(bookId);
		return "r:/lib/book/" + bookId;
	}

	// 8)
	@Delete("{remarkId}")
	public String delete(@Param("bookId") long bookId, @Param("remarkId") long remarkId) {
		remarkService.remove(remarkId);
		return "r:/lib/book/" + bookId;
	}
}