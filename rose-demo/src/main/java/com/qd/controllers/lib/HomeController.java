package com.qd.controllers.lib;

import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 12:16
 */
@LoginRequired
@Path("")
public class HomeController {
	// 1)
	// 这个"1)"表示这个对应于《URI设计》表格中的序号
	@Get
	public String redirect() {
		return "r:/lib/book";
	}
}
