package com.qd.controllers.lib.login;

import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;
import net.paoding.rose.web.annotation.rest.Post;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 12:31
 */
@Path("")
public class LoginController {

	// 9)
	@Get
	public String show() {
		return "@login";
	}

	// 10)
	@Post
	public String doLogin() {
		return "r:/lib/book";
	}
}