package com.qd.controllers.lib.logs;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;

import org.springframework.beans.factory.annotation.Autowired;

import com.qd.controllers.lib.LoginRequired;
import com.qd.lib.service.OperLogService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 12:32
 */
@LoginRequired
@Path("")
public class LogsController {

	@Autowired
	private OperLogService operLogService;

	// 16)
	@Get
	public String list(@Param("offset") int offset) {
		operLogService.find();
		return "@logs-list";
	}
}
