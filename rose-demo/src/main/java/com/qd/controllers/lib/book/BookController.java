package com.qd.controllers.lib.book;

import net.paoding.rose.web.annotation.Param;
import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;
import net.paoding.rose.web.annotation.rest.Post;
import net.paoding.rose.web.annotation.rest.Put;
import net.paoding.rose.web.var.Model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.qd.controllers.lib.LoginRequired;
import com.qd.lib.model.Book;
import com.qd.lib.service.BookService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 12:18
 */
@LoginRequired
@Path("")
public class BookController {

	@Autowired
	private BookService bookService;

	// 2)
	// @Get 表示对空串的Get请求可有此方法处理
	@Get
	public String list(Model model, @Param("limit") int limit) {
		List<Book> bookList = bookService.find(limit);
		model.add("book-list", bookList.toString());
		return "book-list";
	}

	// 3)
	@Post
	public String add(Book book) {

		long id = bookService.save(book);

		if (id > 0) {
			return "r:/lib/book";
		} else {
			return "book-add";
		}
	}

	// 4)
	// @Get("add") 表示对"add"的GET请求，可有此方法处理
	@Get("add")
	public String showAdd() {
		return "book-add";
	}

	// 5)
	// 可以通过大括号表示一个变量，通过冒号说明他的正则规则
	// 正则通常是可以省略的(默认规则是除‘\’的任何的多个字符)，就像后面还有的{remarkId}，虽然remarkId也应该只是数字
	@Get("{bookId:[0-9]+}")
	public String show(Model model, @Param("bookId") long bookId, @Param("edit") boolean isEdit) {
		Book book = bookService.get(bookId);
		model.add("book", book);

		if (isEdit) {
			return "book-edit";
		}
		return "book-page";
	}

	// 6)
	// 这里使用目前浏览器无法支持的@Put，你懂的，详看《URI设计》表格中的《严重说明》:)
	@Put("{bookId:[0-9]+}")
	public String update(@Param("bookId") long bookId, Book book) {
		bookService.modify(book);
		return "r:/lib/book/" + bookId;
	}
}
