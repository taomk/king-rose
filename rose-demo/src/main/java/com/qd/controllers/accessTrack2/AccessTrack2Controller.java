package com.qd.controllers.accessTrack2;

import net.paoding.rose.web.annotation.Intercepted;
import net.paoding.rose.web.annotation.Path;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/17 11:11
 */
@Intercepted(allow = {"accessTrack2"}, deny = {})
@Path("")
public class AccessTrack2Controller {

	public String interceptor() {
		return "@interceptor2";
	}
}
