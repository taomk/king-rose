package com.qd.controllers;

import net.paoding.rose.web.annotation.Path;
import net.paoding.rose.web.annotation.rest.Get;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/15 17:28
 */
@Path("")
public class IndexController {

	@Get
	public String index() {
		return "index";
	}
}
