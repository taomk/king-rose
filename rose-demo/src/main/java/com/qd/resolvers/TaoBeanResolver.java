package com.qd.resolvers;

import net.paoding.rose.web.Invocation;
import net.paoding.rose.web.paramresolver.ParamMetaData;
import net.paoding.rose.web.paramresolver.ParamResolver;

import com.qd.model.Tao;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/17 17:08
 */
public class TaoBeanResolver implements ParamResolver {

	@Override
	public Object resolve(Invocation inv, ParamMetaData metaData) throws Exception {
		for (String paramName : metaData.getParamNames()) {
			if (paramName != null) {
				Tao tao = new Tao();
				String first = inv.getParameter("tao1");
				String second = inv.getParameter("tao2");
				tao.setFirst(first);
				tao.setSecond(second);
				return tao;
			}
		}
		return null;
	}

	@Override
	public boolean supports(ParamMetaData metaData) {
		return Tao.class == metaData.getParamType();
	}
}
