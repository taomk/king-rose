package com.qd.validators;

import java.lang.annotation.*;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/17 23:56
 */
@Inherited
@Target( { ElementType.TYPE, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NotBlank {
}
