package com.qd.validators;

import net.paoding.rose.web.Invocation;
import net.paoding.rose.web.ParamValidator;
import net.paoding.rose.web.paramresolver.ParamMetaData;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/17 23:55
 */
public class NotBlankParamValidator implements ParamValidator {

	@Override
	public boolean supports(ParamMetaData metaData) {
		return metaData.getAnnotation(NotBlank.class) != null;
	}

	@Override
	public Object validate(ParamMetaData metaData, Invocation inv, Object target, Errors errors) {
		String paramName = metaData.getParamName();
		String value = inv.getParameter(paramName);
		if (StringUtils.isBlank(value)) {
			return "@messages is null";
		}
		return null;
	}
}
