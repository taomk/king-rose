package com.qd.model;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/16 22:57
 */
public class Level {

	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
