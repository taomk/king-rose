package com.qd.model;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/17 17:09
 */
public class Tao {

	private String first;

	private String second;

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}
}
