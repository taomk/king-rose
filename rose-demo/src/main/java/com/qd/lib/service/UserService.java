package com.qd.lib.service;

import java.util.List;

import com.qd.lib.model.User;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:22
 */
public interface UserService {

	public User getByLoginName(String loginName);

	public User get(long userId);

	public List<User> find();

	public void remove(long userId);

	public long save(User User);
}
