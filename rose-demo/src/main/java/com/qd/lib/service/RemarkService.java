package com.qd.lib.service;

import java.util.List;

import com.qd.lib.model.Remark;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:19
 */
public interface RemarkService {

	public List<Remark> findByBook(long bookId);

	public void removeByBook(long bookId);

	public int remove(long remarkId);

	public long save(Remark remark);
}
