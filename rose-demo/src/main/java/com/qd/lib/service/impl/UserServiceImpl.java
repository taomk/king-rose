package com.qd.lib.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qd.lib.dao.UserDAO;
import com.qd.lib.model.User;
import com.qd.lib.service.UserService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:22
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public User getByLoginName(String loginName) {
		return userDAO.getByLoginName(loginName);
	}

	@Override
	public User get(long userId) {
		return userDAO.get(userId);
	}

	@Override
	public List<User> find() {
		return userDAO.query();
	}

	@Override
	public void remove(long userId) {
		userDAO.delete(userId);
	}

	@Override
	public long save(User user) {
		return userDAO.insert(user);
	}
}
