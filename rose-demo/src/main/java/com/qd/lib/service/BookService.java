package com.qd.lib.service;

import java.util.List;

import com.qd.lib.model.Book;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:09
 */
public interface BookService {

	public Book get(long bookId);

	public List<Book> find(int limit);

	public List<Book> find(long bookId, int limit);

	public void modify(Book book);

	public long save(Book book);
}
