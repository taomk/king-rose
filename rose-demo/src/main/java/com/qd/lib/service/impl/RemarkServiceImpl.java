package com.qd.lib.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qd.lib.dao.RemarkDAO;
import com.qd.lib.model.Remark;
import com.qd.lib.service.RemarkService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:19
 */
@Service
public class RemarkServiceImpl implements RemarkService {

	@Autowired
	private RemarkDAO remarkDAO;

	@Override
	public List<Remark> findByBook(long bookId) {
		return remarkDAO.queryByBook(bookId);
	}

	@Override
	public void removeByBook(long bookId) {
		remarkDAO.deleteByBook(bookId);
	}

	@Override
	public int remove(long remarkId) {
		return remarkDAO.delete(remarkId);
	}

	@Override
	public long save(Remark remark) {
		return remarkDAO.insert(remark);
	}
}
