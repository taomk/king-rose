package com.qd.lib.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qd.lib.dao.BookDAO;
import com.qd.lib.model.Book;
import com.qd.lib.service.BookService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:09
 */
@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDAO bookDAO;

	@Override
	public Book get(long bookId) {
		return bookDAO.get(bookId);
	}

	@Override
	public List<Book> find(int limit) {
		return bookDAO.query(limit);
	}

	@Override
	public List<Book> find(long bookId, int limit) {
		return bookDAO.query(bookId, limit);
	}

	@Override
	public void modify(Book book) {
		bookDAO.update(book);
	}

	@Override
	public long save(Book book) {
		return bookDAO.insert(book);
	}
}
