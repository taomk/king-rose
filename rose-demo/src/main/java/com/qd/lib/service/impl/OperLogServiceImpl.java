package com.qd.lib.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qd.lib.dao.OperLogDAO;
import com.qd.lib.model.OperLog;
import com.qd.lib.service.OperLogService;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:15
 */
@Service
public class OperLogServiceImpl implements OperLogService {

	@Autowired
	private OperLogDAO operLogDAO;

	@Override
	public List<OperLog> find() {
		return operLogDAO.query();
	}

	@Override
	public long save(OperLog operLog) {
		return operLogDAO.insert(operLog);
	}
}
