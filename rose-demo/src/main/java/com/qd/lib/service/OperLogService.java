package com.qd.lib.service;

import java.util.List;

import com.qd.lib.model.OperLog;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:15
 */
public interface OperLogService {

	public List<OperLog> find();

	public long save(OperLog operLog);
}
