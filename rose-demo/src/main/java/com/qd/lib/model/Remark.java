package com.qd.lib.model;

import java.util.Date;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:16
 */
@Data
public class Remark {

	private long id;

	private String userName;

	private long bookId;

	private String essay;

	private Date createTime;
}
