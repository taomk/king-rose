package com.qd.lib.model;

import java.util.Date;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:20
 */
@Data
public class User {

	private long id;

	private String name;

	private String password;

	private String loginName;

	private Date createTime;
}
