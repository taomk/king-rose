package com.qd.lib.model;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:07
 */
@Data
public class Book {

	private long id;

	private String name;

	private String price;

	private String author;
}
