package com.qd.lib.model;

import java.util.Date;

import lombok.Data;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 13:12
 */
@Data
public class OperLog {

	private long id;

	private String userName;

	private String resourcePattern;

	private String resourceId;

	private int success;

	private String remarks;

	private Date createTime;
}
