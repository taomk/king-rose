package com.qd.lib.dao;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.ReturnGeneratedKeys;
import net.paoding.rose.jade.annotation.SQL;

import java.util.List;

import com.qd.lib.model.Book;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 12:48
 */
// 必须是接口，并且以大写DAO结尾
// 必须标注@DAO，DAO中有一个catalog属性，对于大部分人来说，这个都是没用的
@DAO(catalog = "qd_community_ro")
public interface BookDAO {

	@SQL("select id, name, price, author from book where id = :1")
	public Book get(long bookId);

	@SQL("select id, name, price, author from book order by id desc limit :1")
	public List<Book> query(int limit);

	@SQL("select id, name, price, author from book where id < :1 order by id desc limit :2")
	public List<Book> query(long bookId, int limit);

	@SQL("update book set name=:1.name, price=:1.price, author=:1.author where id=:1.id")
	public void update(Book book);

	@ReturnGeneratedKeys
	@SQL("insert into book (name, price, author) values (:1.name, :1.price, :1.author)")
	public long insert(Book book);

}
