package com.qd.lib.dao;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.ReturnGeneratedKeys;
import net.paoding.rose.jade.annotation.SQL;

import java.util.List;

import com.qd.lib.model.User;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 12:49
 */
@DAO
public interface UserDAO {

	@SQL("select id, name, login_name from user where login_name=:1")
	public User getByLoginName(String loginName);

	@SQL("select id, name, password, login_name, create_time from user where id=:1")
	public User get(long userId);

	@SQL("select id, name, login_name, create_time from user")
	public List<User> query();

	@SQL("delete from user where id=:1")
	public void delete(long userId);

	@ReturnGeneratedKeys
	@SQL("insert into user (name, password, login_name, create_time) values (:1.name, :1.password, :1.loginName, 1.createTime")
	public long insert(User user);
}