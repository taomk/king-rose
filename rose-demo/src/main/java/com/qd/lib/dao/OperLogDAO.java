package com.qd.lib.dao;

import net.paoding.rose.jade.annotation.DAO;
import net.paoding.rose.jade.annotation.ReturnGeneratedKeys;
import net.paoding.rose.jade.annotation.SQL;

import java.util.List;

import com.qd.lib.model.OperLog;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/9/20 12:50
 */
@DAO
public interface OperLogDAO {

	@SQL("select id, user_name, resource_pattern, resource_id, success, remarks, create_time from oper_log")
	public List<OperLog> query();

	@ReturnGeneratedKeys
	@SQL("insert into oper_log (user_name, resource_pattern, resource_id, success, remarks) values (:1.userName, :1.resourcePattern, :1.resourceId, :1.success, :1.remarks)")
	public long insert(OperLog operLog);
}
