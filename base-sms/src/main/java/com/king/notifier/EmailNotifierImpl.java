package com.king.notifier;


import javax.mail.internet.InternetAddress;

import java.util.Collections;
import java.util.List;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EmailNotifierImpl extends AbstractBufferedNotifier implements Notifier {

    private static final String SUBJECT = "服务报警";
    public static final String REPLY_TO = "nuomi.tech@renren-inc.com";

    private final String HOST = "10.3.17.55";

    private static final String FROM_NAME = "糯米通知";
    private static final String FROM = "noreply@nuomi.com";

    private final JavaMailSender mailSender;

    private List<String> emails = Collections.emptyList();

    public EmailNotifierImpl() {
        JavaMailSenderImpl mailSender1 = new JavaMailSenderImpl();
        mailSender1.setHost(HOST);
        this.mailSender = mailSender1;
    }

    @Override
    public void doNotify(String msg) {
        log.info("start sending notify msg [" + msg + "]");
        for (String email : emails) {
            SimpleMailMessage message = new SimpleMailMessage();
            try {
                String fromAddress = new InternetAddress(FROM, FROM_NAME, "utf-8").toString();
                message.setFrom(fromAddress);
                message.setSubject(SUBJECT);
                message.setTo(email);
                message.setText(msg);
                message.setReplyTo(REPLY_TO);
                mailSender.send(message);
            } catch (Exception e) {
                log.error("发送邮件(TEXT)：" + message, e);
            }
        }
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }
}
