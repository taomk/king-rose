package com.king.notifier;

public interface Notifier {
    public void notify(String serviceIdentifier, String msg);
}
