package com.king.hessian;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public interface RpcServer {

    public void handle(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException;
}