package com.king.hessian;

import java.net.URL;
import java.util.Map;

import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;

import com.caucho.hessian.client.HessianConnection;
import com.caucho.hessian.client.HessianConnectionFactory;
import com.caucho.hessian.client.HessianProxyFactory;

/**
 * 使用httpClient访问rpc server的connection factory，通过一个extraHeaders支持可修改的http头信息
 */
public class HttpClientHessianConnectionFactory implements HessianConnectionFactory {

    private Map<String, String> extraHeaders = null;

    private HttpClient httpClient;

    public HttpClientHessianConnectionFactory() {
        Scheme http = new Scheme("http", PlainSocketFactory.getSocketFactory(), 80);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(http);
        HttpParams httpParams = new BasicHttpParams();
        httpParams.setParameter(CoreProtocolPNames.USER_AGENT, "Hessian/HttpClient");
        ClientConnectionManager connMrg = new ThreadSafeClientConnManager(httpParams, schemeRegistry);
        httpClient = new DefaultHttpClient(connMrg, null);
        httpClient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
        httpClient.getParams().setParameter(CoreProtocolPNames.USE_EXPECT_CONTINUE, Boolean.FALSE);
        httpClient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Hessian/HttpClient");
        httpClient.getParams().setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 50);
    }

    @Override
    public void setHessianProxyFactory(HessianProxyFactory hessianProxyFactory) {
        int readTimeout = (int) hessianProxyFactory.getReadTimeout();
        int connectTimeout = (int) hessianProxyFactory.getConnectTimeout();
        if (readTimeout > 0) {
            httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, readTimeout);
        }
        if (connectTimeout > 0) {
            httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectTimeout);
        }
    }

    @Override
    public HessianConnection open(URL url) throws java.io.IOException {
        HessianConnection connection = new HttpClientHessianConnection(httpClient, url);
        if (extraHeaders != null) {
            for (Map.Entry<String, String> entry : extraHeaders.entrySet()) {
                if (entry.getKey() != null && entry.getValue() != null) {
                    connection.addHeader(entry.getKey(), entry.getValue());
                }
            }
        }
        return connection;
    }

    public Map<String, String> getExtraHeaders() {
        return extraHeaders;
    }

    public void setExtraHeaders(Map<String, String> extraHeaders) {
        this.extraHeaders = extraHeaders;
    }

}

