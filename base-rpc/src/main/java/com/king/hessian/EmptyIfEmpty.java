package com.king.hessian;

import java.lang.annotation.*;

/**
 * 标识指定参数位上的实参若为空集合/空列表/空map，则无需进行Rpc直接在本地返回一个空集合/空列表/空map。具体行为请参见EmptyIfEmptyMethodInterceptor
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EmptyIfEmpty {

    int indexOfParam() default 0;
}
