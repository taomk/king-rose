package com.king.hessian;

import java.lang.annotation.*;

/**
 * 暴露一个bean的所有方法为rpc的注解，由RpcFilter(as BeanPostProcessor处理)
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Rpc {

    public String[] url();

    public Class<?> serviceInterface();

}
