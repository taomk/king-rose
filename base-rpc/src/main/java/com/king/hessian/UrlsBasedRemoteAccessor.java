package com.king.hessian;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.remoting.support.RemoteAccessor;
import org.springframework.util.Assert;

/**
 * 模拟spring的UrlBasedRemoteAccessor，提供支持a list of urls的接口
 */
public class UrlsBasedRemoteAccessor extends RemoteAccessor implements InitializingBean {

    private List<String> serviceUrls;

    public List<String> getServiceUrls() {
        return serviceUrls;
    }

    public void setServiceUrls(List<String> serviceUrls) {
        this.serviceUrls = serviceUrls;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(getServiceUrls(), "The property 'serviceUrls' is required");
    }
}