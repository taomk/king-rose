package com.king.hessian;

import java.lang.annotation.*;

/**
 * 标识某Rpc接口可在本地缓存，建议后期持续改进此接口已提供可配置，统一的缓存机制。目前只适合一些跨服务器常量Rpc，例如category/city等
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Cacheable {

}
