package com.king.hessian;


import com.qd.able.Refreshable;

/**
 * 支持url动态刷新的接口，在url刷新后，ServerMapping会调用refresh方法
 */
public interface DynamicUrlSupport extends Refreshable {

    public void refresh();
}
