package com.king.hessian;

import java.net.InetAddress;
import java.net.UnknownHostException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RpcClientInfo {

    public static final String HTTP_HEADER_X_RPC_CLIENT = "X-Rpc-Client";

    public static final String DEFAULT_CLIENT = "unknown";

    private static final String CLIENT;

    private static final ThreadLocal<String> CURRENT_CLIENT = new ThreadLocal<String>();

    static {
        String hostName = "";
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            log.warn("无法获取hostname: " + e.getMessage(), e);
        } catch (Exception e) {
            log.warn("无法获取hostname: " + e.getMessage(), e);
        }
        CLIENT = hostName;
    }

    public static final String getServerName() {
        return CLIENT;
    }

    public static final void setCurrentClient(String client) {
        CURRENT_CLIENT.set(client);
    }

    public static final void removeCurrentClient() {
        CURRENT_CLIENT.remove();
    }

    public static final String getCurrentClient() {
        return CURRENT_CLIENT.get();
    }

}
